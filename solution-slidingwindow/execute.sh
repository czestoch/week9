#!/bin/bash
if [[ $# -lt 2 ]]; then
        echo "Usage: $0 [HOST] [PORT]"
        echo "Provide the endpoint address of the data source."
        exit
fi

log4j_setting="-Dlog4j.configuration=file:log4j.properties"
spark-submit \
             --conf "spark.driver.extraJavaOptions=${log4j_setting}" \
             --conf "spark.executor.extraJavaOptions=${log4j_setting}" \
             --class "org.apache.spark.examples.streaming.NetworkWordCount" \
             --master local[*] \
             --files "$(pwd)/log4j.properties" \
            target/scala-2.11/spark-streaming-word-count_2.11-1.0.jar \
            $1 $2
